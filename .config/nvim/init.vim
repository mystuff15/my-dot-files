
:set number
:set hlsearch
:set ic







call plug#begin()

Plug 'LnL7/vim-nix'
Plug 'dkarter/bullets.vim'
Plug 'kyazdani42/nvim-web-devicons'
Plug 'psliwka/vim-smoothie'
Plug 'nvim-lualine/lualine.nvim'
Plug 'tpope/vim-commentary' 

call plug#end()
