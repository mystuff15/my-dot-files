#!/bin/bash

#terminate running polybar
killall -q polybar

#wait for it to shut down
polybar-msg cmd quit

polybar top &

