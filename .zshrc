


# The following lines were added by compinstall

zstyle ':completion:*' completer _expand _complete _ignored _match _correct _approximate
zstyle ':completion:*' list-colors ''
zstyle ':completion:*' max-errors 3
zstyle :compinstall filename '/home/troll/.zshrc'

autoload -Uz compinit
compinit -d ~/.config/zsh/zcompdump
# End of lines added by compinstall
# Lines configured by zsh-newuser-install
HISTFILE=~/.config/zsh/.histfile
HISTSIZE=15000
SAVEHIST=15000
setopt autocd beep nomatch
bindkey -v
# End of lines configured by zsh-newuser-install
#makes it show the full path rather then just hostname%
#export PS1="[%~] > "

#aliases 
alias wget='wget -c'
alias ping='ping -c 5'
alias reboot='sudo reboot'
alias mkdir='mkdir -pv'
alias icat='kitty +kitten icat'
alias ls='ls --color=auto'
alias poweroff='sudo poweroff'
alias la="ls -a"
alias v="sudo -E nvim"

#some useful plugins
#source ~/.config/zsh/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh
source ~/.config/zsh/zsh-autosuggestions.zsh
source ~/.config/zsh/zsh-history-substring-search.zsh
bindkey '^[[A' history-substring-search-up
bindkey '^[[B' history-substring-search-down
#source ~/.config/zsh/auto-notify.plugin.zsh

 eval "$(starship init zsh)"
