#!/bin/bash
#this is a simple install script i made i wouldn't really recommend using it this is just for my personal use

read -p "are you ready to use the install script y/n " answer

if [ $answer != y ]; then
	echo "exiting goodbye"
	exit 0
fi

echo "very well continuing"

lsblk
read -p "what is your partition name ex. sda sdb etc" type

echo "please partition drives"
echo "1 = boot 2 = swap 3 = root"
sleep 3
cfdisk
sleep 1

#formatting
mkfs.ext4 /dev/"$type"3 || echo "failed to format root code 1" && exit 1
mkswap /dev/"$type"2 || echo "failed to format swap code 2" && exit 1
mkfs.fat -F 32 /dev/"$type"1 || echo "failed to format uefi boot code 3" && exit 1

#mounting
swapon /dev/"$type"2 || echo "failed to swapon code 4" && exit 1

mount /dev/"$type"3 /mnt || echo "failed to swapon " && exit 1

mkdir /mnt/boot || echo "failed to mkdir /mnt/boot" && exit 1

mount /dev/"$type"1 /mnt/boot || echo "failed to mount boot partition code 6" && exit 1

#running reflector
read -p "what country are you from" place
read -p "what protocol do you want to use if multiple seperate with comma ex https,http" protocol
reflector --sort rate --country "$place" --protocol "$protocol" --save /etc/pacman.d/mirrorlist

#getting base system packages
pacstrap /mnt base base-devel linux-zen linux-firmware || echo "download failed exiting" && exit 1

#generating fstab
fstabgen -U /mnt >> /mnt/etc/fstab || echo "fstab failed to generate code 9" && exit 1
vi /mnt/etc/fstab
sleep 1
