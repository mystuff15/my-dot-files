#!/bin/bash 
#refer to part 1

#setting timezone
ln -sf /usr/share/zoneinfo/US/Central /etc/localtime
hwlock --systohc

#setting up locales and installing neovim and zsh
pacman -S --disable-download-timeout neovim zsh
echo "en_US.UTF-8" >> /etc/locale.gen
locale-gen
export LANG="en_US.UTF-8" 
export LC_COLLATE="C"

#setting up bootloader
pacman -S --disable-download-timeout grub efibootmgr
grub-install --target=x86_64-efi --efi-directory=/boot --bootloader-id=grub --removable
grub-mkconfig -o /boot/grub/grub.cfg
	
#adding my user
useradd -m -G wheel -s /bin/zsh troll

#i would do this part automatically but im a bit lazy and im afraid of the formatting getting screwed up because of it
echo "artix" > /etc/hostname
echo "plase enter matching entries to host"
nvim /etc/hosts
sleep 3

pacman -S --disable-download-timeout dhcpcd dhclient connman-runit connman-gtk sddm-runit git
ln -s /etc/runit/sv/connmand /etc/runit/runsvdir/default

echo "done please enter your root password with passwd"
